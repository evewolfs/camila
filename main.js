var Quiz = function(){
    var self = this;
    this.init = function(){
      self._bindEvents();
    }
    
    this.correctAnswers = [
      { question: 1, answer: 'b' },
      { question: 2, answer: 'd' },
      { question: 3, answer: 'd' },
      { question: 4, answer: 'c' },
      { question: 5, answer: 'a' },
      { question: 6, answer: 'b' },
    ]
    
    this._pickAnswer = function($answer, $answers){
      $answers.find('.quiz-answer').removeClass('active');
      $answer.addClass('active');
    }
    this._calcResult = function(){
      var numberOfCorrectAnswers = 0;
      $('ul[data-quiz-question]').each(function(i){
        var $this = $(this),
            chosenAnswer = $this.find('.quiz-answer.active').data('quiz-answer'),
            correctAnswer;
        
        for ( var j = 0; j < self.correctAnswers.length; j++ ) {
          var a = self.correctAnswers[j];
          if ( a.question == $this.data('quiz-question') ) {
            correctAnswer = a.answer;
          }
        }
        
        if ( chosenAnswer == correctAnswer ) {
          numberOfCorrectAnswers++;
          
          // highlight this as correct answer
          $this.find('.quiz-answer.active').addClass('correct');
        }
        else {
          $this.find('.quiz-answer[data-quiz-answer="'+correctAnswer+'"]').addClass('correct');
          $this.find('.quiz-answer.active').addClass('incorrect');
        }
      });
      if ( numberOfCorrectAnswers < 3 ) {
        return {code: 'bad', text: 'Aie non ça craint, recommence pour un meilleur Kdo'};
      }
      else if ( numberOfCorrectAnswers == 3 || numberOfCorrectAnswers == 4 ) {
        return {code: 'mid', text: 'Presque prête, recommence pour un meilleur Kdo'};
      }
      else if ( numberOfCorrectAnswers > 4 ) {
        return {code: 'good', text: 'Bravo, tu es fin prête, découvre ton Kdo sans plus tarder!'};
      }
    }
    this._isComplete = function(){
      var answersComplete = 0;
      $('ul[data-quiz-question]').each(function(){
        if ( $(this).find('.quiz-answer.active').length ) {
          answersComplete++;
        }
      });
      if ( answersComplete >= 6 ) {
        return true;
      }
      else {
        return false;
      }
    }
    this._showResult = function(result){
      $('.quiz-result').addClass(result.code).html(result.text);
    }
    this._bindEvents = function(){
      $('.quiz-answer').on('click', function(){
        var $this = $(this),
            $answers = $this.closest('ul[data-quiz-question]');
        self._pickAnswer($this, $answers);
        if ( self._isComplete() ) {
          
          // scroll to answer section
          $('html, body').animate({
            scrollTop: $('.quiz-result').offset().top
          });
          
          self._showResult( self._calcResult() );
          $('.quiz-answer').off('click');
          
        }
      });
    }
  }
  var quiz = new Quiz();
  quiz.init();

  
//Radio button
$('input[type="radio"]').click(function(){
  if($(this).attr("value")=="ABC"){
      $(".Box").hide('slow');
  }
  if($(this).attr("value")=="PQR"){
      $(".Box").show('slow');

  }        
});
$('input[type="radio"]').trigger('click');